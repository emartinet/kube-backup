FROM alpine:3.14

RUN apk update && \
  apk add --update \
    bash \
    easy-rsa \
    git \
    openssh-client \
    curl \
    ca-certificates \
    jq \
    python3 \
    py3-yaml \
    libstdc++ \
    gpgme \
    git-crypt \
    && \
  rm -rf /var/cache/apk/*

RUN adduser -h /backup -D backup

ARG KUBECTL_VERSION="1.21.4"
ARG KUBECTL_SHA256="9410572396fb31e49d088f9816beaebad7420c7686697578691be1651d3bf85a"

RUN curl -SL \
  "https://dl.k8s.io/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /usr/local/bin/kubectl && \
  chmod +x /usr/local/bin/kubectl
RUN echo "${KUBECTL_SHA256}  /usr/local/bin/kubectl" | sha256sum -c - || exit 10

COPY entrypoint.sh /
USER backup
ENTRYPOINT ["/entrypoint.sh"]
